// Відповіді на теоретичні питання
// 1. Які існують типи даних у Javascript?
/* У JS існує 8 основних типів даних:
-String (строка)
-number  (число)
-boolean (true(1) або false(0))
-object
-undefined
-Null
-BigInt
-Symbol */

// 2. У чому різниця між == і ===?
/* == та === - оператори порівняння, різниця у суворості,
== виконує приведення типів 1 == 1 (number = number),s = S(String = String), в результаті отримаємо true
===  порівнює величини , на ідентичність і не приводить значення до одного типу на відміу від ==
 */
// 3.Що таке оператор?
//  Оператор - це внутрішня функція JS яка запускає якусь вбудовану функцію, в результаті остання повертає результат

// Задача №1
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
let userName = prompt(`Enter your name`).trim();//Отримуємо Ім'я користувача, записуємо у змінну userName
let userAge = +prompt(`Enter your age`).trim();//Отримуємо вік користувача, записуємо у змінну userName, також явно перетворюємо в тип даних number

// Перевірка на коректність введених даних
if (userName == "" || isNaN(userAge) || userAge == "") {
    userName = prompt(`Enter your name`);
    userAge = +prompt(`Enter your age`);
}

// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.

if (userAge < 18) {
    alert(`You are not allowed to visit this website.`);
}

// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel. 
// Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача.
// Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.

else if (userAge >= 18 && userAge <= 22) {
    let answerAge = confirm(`Are you sure you want to continue?`)
    if (answerAge === true) {
        alert(`Welcome, ${userName}.`);
    } else {
        alert(`You are not allowed to visit this website.`);
    }
}

// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
else if (userAge > 22) {
    alert(`Welcome, ${userName}.`);
}
